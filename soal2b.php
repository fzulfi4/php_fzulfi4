<html>

<head>
    <title></title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <header>Signup Form</header>
        <div class="progress-bar">
            <div class="step">
                <p>Hasil</p>
                <div class="bullet">
                    <span>4</span>
                </div>
                <div class="check fas fa-check"></div>
            </div>
        </div>
        <div class="form-outer">
            <form action="soal2a.php" method="post">
                
                <div class="page">
                    <div class="title">Hasil</div>
                    <div class="field">
                        <div class="label">Nama</div>
                        <div class="card"><?php echo $_POST["nama"]; ?></div>
                    </div>
                    <div class="field">
                        <div class="label">Umur</div>
                        <div class="card"><?php echo $_POST["umur"]; ?></div>
                    </div>
                    <div class="field">
                        <div class="label">Hobi</div>
                        <div class="card"><?php echo $_POST["hobi"]; ?></div>
                    </div>
                    <div class="field btns">
                        <button class="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        initMultiStepForm();

        function initMultiStepForm() {
            const progressNumber = document.querySelectorAll(".step").length;
            const slidePage = document.querySelector(".slide-page");
            const submitBtn = document.querySelector(".submit");
            const progressText = document.querySelectorAll(".step p");
            const progressCheck = document.querySelectorAll(".step .check");
            const bullet = document.querySelectorAll(".step .bullet");
            const pages = document.querySelectorAll(".page");
            const nextButtons = document.querySelectorAll(".next");
            const prevButtons = document.querySelectorAll(".prev");
            const stepsNumber = pages.length;

            if (progressNumber !== stepsNumber) {
                console.warn(
                    "Error, number of steps in progress bar do not match number of pages"
                );
            }

            document.documentElement.style.setProperty("--stepNumber", stepsNumber);

            let current = 1;

            for (let i = 0; i < nextButtons.length; i++) {
                nextButtons[i].addEventListener("click", function(event) {
                    event.preventDefault();

                    inputsValid = validateInputs(this);
                    // inputsValid = true;

                    if (inputsValid) {
                        slidePage.style.marginLeft = `-${
                    (100 / stepsNumber) * current
                }%`;
                        bullet[current - 1].classList.add("active");
                        progressCheck[current - 1].classList.add("active");
                        progressText[current - 1].classList.add("active");
                        current += 1;
                    }
                });
            }

            for (let i = 0; i < prevButtons.length; i++) {
                prevButtons[i].addEventListener("click", function(event) {
                    event.preventDefault();
                    slidePage.style.marginLeft = `-${
                (100 / stepsNumber) * (current - 2)
            }%`;
                    bullet[current - 2].classList.remove("active");
                    progressCheck[current - 2].classList.remove("active");
                    progressText[current - 2].classList.remove("active");
                    current -= 1;
                });
            }
            submitBtn.addEventListener("click", function() {
                bullet[current - 1].classList.add("active");
                progressCheck[current - 1].classList.add("active");
                progressText[current - 1].classList.add("active");
                current += 1;
                setTimeout(function() {
                    alert("Data brhasil DI input");
                    location.reload();
                }, 800);
            });

            function validateInputs(ths) {
                let inputsValid = true;

                const inputs =
                    ths.parentElement.parentElement.querySelectorAll("input");
                for (let i = 0; i < inputs.length; i++) {
                    const valid = inputs[i].checkValidity();
                    if (!valid) {
                        inputsValid = false;
                        inputs[i].classList.add("invalid-input");
                    } else {
                        inputs[i].classList.remove("invalid-input");
                    }
                }
                return inputsValid;
            }
        }
    </script>
</body>

</html>