<?php
include 'config.php';
?>

<!DOCTYPE html>
<html>

<head>
    <title>soal 3</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css" />
</head>

<body>
    <div class="text-center">
        <h2>Data</h2>
    </div>

    <div class="container">
        <?php
        include "config.php";
        $query = mysqli_query($koneksi, "select person.nama,person.alamat,hobi.hobi from person join hobi ON hobi.person_id = person.id");
        $count = mysqli_num_rows($query);
        ?>
        <table id="example" class="table table-striped table-bordered " style="width:100%">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Hobi</th>
                </tr>
            </thead>
            <tbody>
                <?php if (mysqli_num_rows($query) > 0) { ?>
                    <?php
                    $no = 1;
                    while ($data = mysqli_fetch_array($query)) {
                    ?>
                        <tr>
                            <th scope="row"><?php echo $no; ?></th>
                            <td><?php echo $data['nama']; ?></td>
                            <td><?php echo $data['alamat']; ?></td>
                            <td><?php echo $data['hobi']; ?></td>
                        </tr>
                    <?php $no++;
                    } ?>
                <?php } ?>
            </tbody>
        </table>
    </div>



    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"> </script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"> </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>

</body>

</html>