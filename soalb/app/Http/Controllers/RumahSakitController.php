<?php

namespace App\Http\Controllers;

use App\Models\Rumahsakit;
use App\Models\Pasien;
use Illuminate\Http\Request;

class RumahsakitController extends Controller
{
    public function index()
    {
        $pasien = Pasien::join('rumah_sakits', 'pasiens.rumahsakit_id', '=', 'rumah_sakits.id')
            ->get('*');

        return view('home', ['pasien' => $pasien]);
    }
}
