<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rumahsakit extends Model
{
    use HasFactory;
    protected $table = 'rumah_sakits';
    protected $fillable = ['nama', 'alamat', 'email', 'notlp'];
}
