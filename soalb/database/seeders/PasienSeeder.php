<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class PasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pasiens')->insert([
            'nama' => 'gilang',
            'alamat' => 'majalaya',
            'notlp' => '08964235452',
            'rumahsakit_id' => '1',
        ]);
    }
}
