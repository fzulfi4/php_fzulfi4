<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class RumahSakitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rumah_sakits')->insert([
            'nama_rumahsakit' => 'rumahsakit',
            'alamat_rumahsakit' => 'majalaya',
            'email_rumahsakit' => 'rumahsakit@gmail.com',
            'notlp_rumahsakit' => '089653891348',
        ]);
    }
}
