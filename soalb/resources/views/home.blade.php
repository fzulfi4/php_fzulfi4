@extends('master')

@section('container')
<div class="text-center">
<h5>Data Pasien & Rumah Sakit</h5>
</div>
  {{-- <a class="btn btn-success" href="{{route('tambahrumahsakit')}}"><i class="fa fa-plus"></i> Tambah Rumah Sakit</a><br><br> --}}
  <table class="table table-bordered table-hover">
    <tr>
      <th>#ID</th>
      <th>Nama</th>
      <th>Alamat</th>
      <th>No Telepon</th>
      <th>Nama Rumah Sakit</th>
    </tr>
    @foreach($pasien as $s)
    <tr>
      <td>{{$s->id}}</td>
      <td>{{$s->nama}}</td>
      <td>{{$s->alamat}}</td>
      <td>{{$s->notlp}}</td>
      <td>{{$s->nama_rumahsakit}}</td>

      {{-- <td>
        <a href="/santri/ubah/{{$s->id_santri}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
        <a href="/santri/hapus/{{$s->id_santri}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
      </td> --}}
    </tr>
    @endforeach
  </table>
  <div class="text-center">
    <h5>Data Rumah Sakit</h5>
    </div>
  <table class="table table-bordered table-hover">
    <tr>
      <th>#ID</th>
      <th>Nama</th>
      <th>Alamat</th>
      <th>Email</th>
      <th>No Telepon</th>
    </tr>
    @foreach($pasien as $r)
    <tr>
      <td>{{$r->id}}</td>
      <td>{{$r->nama_rumahsakit}}</td>
      <td>{{$r->alamat_rumahsakit}}</td>
      <td>{{$r->email_rumahsakit}}</td>
      <td>{{$r->notlp_rumahsakit}}</td>

      {{-- <td>
        <a href="/santri/ubah/{{$s->id_santri}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
        <a href="/santri/hapus/{{$s->id_santri}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
      </td> --}}
    </tr>
    @endforeach
  </table>
  @endsection
